const express = require("express")
const Post = require("../models/post")

exports.updatePost =  (req, res)=>{
    let imagePath = req.body.imagePath;
    if(req.file){
        const url = req.protocol + '://' + req.get('host');
        imagePath = url + '/images/'+ req.file.filename
    }
    const post =Post({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content,
        imagePath: imagePath,
        creater: req.userData.userID
    });
    Post.updateOne({ _id: req.params.id , creater:req.userData.userID }, post).then(result =>{
    if(result.modifiedCount>0){
        res.status(200).json({message:"Update Successfull !!!" });
    }else{
        res.status(401).json({
            message:"Not authorized."
        })
    }
    }).catch(error => {
        res.status(500).json({
            message:"Coudn't Update Post"
        })
    })
}

exports.createPost =  (req, res)=>{
    const url = req.protocol + '://' + req.get('host');
    const post = new Post({
        title: req.body.title,
        content: req.body.content,
        imagePath: url + '/images/'+ req.file.filename,
        creater: req.userData.userID
    });
    console.log(req.userData);
    // return res.status(200).json({})
    post.save()
    .then(createdPost =>{
        res.status(201).json({
            message:"Post Added Successfully",
            post:{
                id: createdPost._id,
                title: createdPost.title,
                content: createdPost.content,
                imagePath: createdPost.imagePath 
            }
        })
    }).catch(error =>{
        res.status(500).json({
            message:"Post Creation Failed!"
        })
    })
}

exports.getPost = (req, res, next)=>{
    Post.find()
    .then(documents =>{
        res.status(200).json({
            message:'Post fetched Successfull',
            posts:documents
        })
    }).catch(error =>{
        res.status(500).json({
            message:'Fetching Posts Failed!'
        })
    })
    
}
exports.getPostById =  (req, res, next)=>{
    Post.findById(req.params.id).then(post =>{
        if(post){
            res.status(200).json(post)
        }else{
            res.status(400).json({message:'Post Not Found!'})
        }
    }).catch(error =>{
        res.status(500).json({
            message:"Fetching Post Failed"
        })
    })
}

exports.DeletePost = (req, res, next)=>{
    Post.deleteOne({_id: req.params.id, creater:req.userData.userID}).then(
    result =>{
    if(result.deletedCount>0){
        res.status(200).json({
            message:"Post Deleted."
        })
    }else{
        res.status(401).json({
            message:"Unauthorised User to delete the post."
        })
    }
    }).catch(error =>{
        res.status(500).json({
            message:"Post Deletion Failed."
        })
    })
}