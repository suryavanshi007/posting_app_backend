const express = require('express');
const path = require('path');
const app = express();
const cors= require('cors')
const mongoose= require('mongoose')
const postsRoutes = require("./routes/posts")
const userRoutes = require("./routes/user")

app.use(express.json());
app.use(cors());
app.use('/images',express.static(path.join("backend/images")))

mongoose.connect("mongodb+srv://krishna07:"+ process.env.MONGO_ATLAS_PASS +"@cluster0.gjx11.mongodb.net/firstdatabase?retryWrites=true&w=majority")
.then(()=>{
    console.log("Connected to Database Succsssfully");
}).catch(()=>{
    console.log("Connection failed");
})

app.use(postsRoutes);
app.use(userRoutes);

app.listen(3001, ()=>{
    console.log("Server is running on port http://localhost:3001");
})
