const express = require("express")
const router = express.Router();
const multer = require("multer");
const checkauth = require("../middleware/check-auth");
const Post = require("../models/post")
const postController = require("../controllers/posts")
// require("dotenv/config");
// const api = process.env.APIPOST_URL;

const MIME_TYPE_MAP= {
    'image/png':'png',
    'image/jpeg':'jpg',
    'image/jpg':'jpg'
}
const storage= multer.diskStorage({
    destination: (req, file, cb)=>{
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = new Error("Invalid mime type");
        if(isValid){
            error = null;
        }
        cb(error, "backend/images");
    },
    filename: (req, file, cb)=>{
        const name= file.originalname.toLowerCase().split(' ').join('-');
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name + '-'+ Date.now()+ '.'+ext);
    }
})

router.put("/api/posts/:id",checkauth,
 multer({storage: storage}).single("image"),
 postController.updatePost
);

router.post("/api/posts",checkauth ,
multer({storage: storage}).single("image"),
postController.createPost);

router.get("/api/posts", postController.getPost)
router.get("/api/posts/:id", postController.getPostById)

router.delete("/api/posts/:id", checkauth,postController.DeletePost)
module.exports = router;