const express = require("express")
const router = express.Router();
const User = require("../models/user")
const bcrypt= require("bcrypt")
const jwt = require("jsonwebtoken");
const userController= require("../controllers/user")

router.post('/api/user/signup',userController.createUser);

router.post('/api/user/login',userController.userLogin);
module.exports = router;