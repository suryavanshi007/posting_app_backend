const jwt= require('jsonwebtoken')
const User = require("../models/user") 
const Post = require("../models/post")
module.exports = (req, res, next)=>{
    try{
        const token = req.headers.authorization.split(" ")[1];
        const decodedToken = jwt.verify(token , process.env.JWT_KEY)
        req.userData = {email:decodedToken.email, userID:decodedToken.userid}
        next();
    } catch (error) {
        res.status(401).json({
            message: "You're Not Authenticated!"
        });
    }
}